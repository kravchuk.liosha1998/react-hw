import React from 'react'
import {Form, Formik, Field} from 'formik'
import * as Yup from 'yup'
import './TrashForm.scss'
import TrashInput from '../TrashInput/TrashInput'
import {useDispatch} from 'react-redux'
import {getTrash, deleteTrash} from '../../store/operations'
import {SUBMIT_FORM} from '../../store/types'

const validationSchema = Yup.object().shape({
    userName: Yup.string().required('Name is required'),
    userSurname: Yup.string().required('Surname is required'),
    userAge: Yup.number().required('Age is required'),
    userAddress: Yup.string().required('Address is required'),
    userPhone: Yup.number().required('Phone is required'),
})

const TrashForm = () => {
    const dispatch = useDispatch()
    
    const setUser = (userInfo, trash) => {
        dispatch({
            type: SUBMIT_FORM,
            payload: { userInfo: userInfo, trash: trash },
        })
    }
    
    const submitForm = (
        { userName, userSurname, userAge, userAddress, userPhone },
        { resetForm }
    ) => {
        const userInfo = {
            userName: userName,
            userSurname: userSurname,
            userAge: userAge,
            userAddress: userAddress,
            userPhone: userPhone,
        }
        const trash = getTrash()
        
        setUser(userInfo, trash)
        deleteTrash()
        resetForm({})
    
        console.log('Trash:', trash)
        console.log('UserInfo:', userInfo)
    }
    
    return (
        <div>
            <div className='case-name'><h3>Enter info for delivery</h3></div>
            <hr className='case__line'></hr>
            <Formik
                initialValues={{
                    userName: '',
                    userSurname: '',
                    userAge: '',
                    userAddress: '',
                    userPhone: '',
                }}
                onSubmit={submitForm}
                validationSchema={validationSchema}
                validateOnChange={false}
                validateOnBlur={false}
            >
                {() => {
                    return (
                        <div>
                            <Form>
                                <Field
                                    component={TrashInput}
                                    name='userName'
                                    type='text'
                                    placeholder='Name'
                                />
                                <Field
                                    component={TrashInput}
                                    name='userSurname'
                                    type='text'
                                    placeholder='Surname'
                                />
                                <Field
                                    component={TrashInput}
                                    name='userAge'
                                    type='number'
                                    placeholder='Age'
                                />
                                <Field
                                    component={TrashInput}
                                    name='userAddress'
                                    type='text'
                                    placeholder='Order address'
                                />
                                <Field
                                    component={TrashInput}
                                    name='userPhone'
                                    type='number'
                                    placeholder='Phone number'
                                />
                                <button className='form-submit' type='submit'>Checkout</button>
                            </Form>
                        </div>
                    )
                }}
            </Formik>
        </div>
    )
}

export default TrashForm
