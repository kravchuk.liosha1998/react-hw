import React from 'react'
import Card from '../../components/Card/Card'
import { useSelector } from 'react-redux'

function Favorites() {
    const items = useSelector((state) => state.items.data)
    const favourites = items.filter((item) => item.favourite)
    
   
    return (
        <div className='container'>
            {favourites.map((item) => {
                return <Card key={item.article} item={item} toTrash />
            })}
        </div>
    )
}

export default Favorites
