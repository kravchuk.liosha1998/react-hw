import React from 'react'
import './TrashInput.scss'

const TrashInput = (props) => {
    const { type, placeholder, form, field } = props
    const { name } = field
    return (
        <div>
            <label>
                {form.errors[name] && form.touched[name] && (
                    <div className='input-error'>{form.errors[name]}</div>
                )}
                <input placeholder={placeholder} type={type} {...field} />
            </label>
        </div>
    )
}

export default TrashInput
