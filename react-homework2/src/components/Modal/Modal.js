import React from 'react'
import './Modal.scss'
import PropTypes from 'prop-types'

const Modal = (props) => {
    const { header, closeButton, closeButtonOnClick, text, actions } = props
    
    return (
        <div className='modal-background' onClick={closeButtonOnClick}>
            <div className='modal__content' onClick={(e) => e.stopPropagation()}>
                <header className='modal__content-header'>
                    <div>{header}</div>
                    {closeButton && (
                        <div onClick={closeButtonOnClick} className='modal__close-btn'>
                            &times;
                        </div>
                    )}
                </header>
                <div>
                    <p className='modal__content-text'>{text}</p>
                    <div className='modal__buttons'>{actions}</div>
                </div>
            </div>
        </div>
    )
}

export default Modal

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
}
