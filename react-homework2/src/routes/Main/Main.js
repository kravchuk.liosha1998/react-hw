import React from 'react'
import Card from '../../components/Card/Card'
import './Main.scss'

const Main = ({
  items,
  saveThisItem,
  openModal,
  addToFavourite,
  modalAction,
  favorites,
}) => {
  return (
    <div className='container'>
      {items.map((item) => {
        return (
          <Card
            favourite={favorites.includes(item.article)}
            modalAction={modalAction}
            key={item.article}
            item={item}
            saveThisItem={saveThisItem}
            openModal={openModal}
            addToFavourite={addToFavourite}
            toTrash
          />
        )
      })}
    </div>
  )
}

export default Main
