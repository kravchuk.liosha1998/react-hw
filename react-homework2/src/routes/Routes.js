import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Main from "./Main/Main";
import Page404 from "./Page404/Page404";

const Routes = ({
  items,
  saveThisItem,
  openModal,
  addToFavourite,
  modalAction,
  deleteCurrentProductFromCart,
  favorites,
}) => {
  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/main" />
        <Route
          exact
          path="/main"
          render={() => {
            return (
              <Main
                modalAction={modalAction}
                items={items}
                favorites={favorites}
                saveThisItem={saveThisItem}
                openModal={openModal}
                addToFavourite={addToFavourite}
                toTrash
              />
            );
          }}
        />
        <Route path="*" component={Page404} />
      </Switch>
    </>
  );
};

export default Routes;
