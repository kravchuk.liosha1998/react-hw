import React from 'react'
import {Route, Redirect, Switch} from 'react-router-dom'
import Main from './Main/Main'
import Trash from './Trash/Trash'
import Favourites from './Favourites/Favourites'
import Page404 from './Page404/Page404'

const Routes = ({
                    items,
                    saveThisItem,
                    deleteThisItem,
                    openModal,
                    addToFavourite,
                    modalAction,
                    favourites,
                }) => {
    return (
        <>
            <Switch>
                <Redirect exact from='/' to='/main'/>
                <Route
                    exact
                    path='/main'
                    render={() => {
                        return (
                            <Main
                                modalAction={modalAction}
                                items={items}
                                favourites={favourites}
                                saveThisItem={saveThisItem}
                                openModal={openModal}
                                addToFavourite={addToFavourite}
                                toTrash
                            />
                        )
                    }}
                />
                <Route
                    exact
                    path='/trash'
                    render={() => (
                        <Trash
                            modalAction={modalAction}
                            items={items}
                            favourites={favourites}
                            saveThisItem={saveThisItem}
                            deleteThisItem={deleteThisItem}
                            openModal={openModal}
                            addToFavourite={addToFavourite}
                        />
                    )}
                />
                <Route
                    exact
                    path="/favourites"
                    render={() => (
                        <Favourites
                            items={items}
                            favourites={favourites}
                            modalAction={modalAction}
                            saveThisItem={saveThisItem}
                            openModal={openModal}
                            addToFavourite={addToFavourite}
                        />
                    )}
                />
                <Route path='*' component={Page404}/>
            </Switch>
        </>
    )
}

export default Routes
