import React from 'react'
import Card from '../../components/Card/Card'

function Favourites({
                        items,
                        modalAction,
                        saveThisItem,
                        openModal,
                        addToFavourite,
                        favourites,
                    }) {
    
    if (!favourites.length) {
        return <div className='container'>Empty</div>
    }
    
    return (
        <div className='container'>
            {items
                .filter((item) => favourites.includes(item.article))
                .map((item) => {
                    return (
                        <Card
                            favourite
                            modalAction={modalAction}
                            key={item.article}
                            item={item}
                            saveThisItem={saveThisItem}
                            openModal={openModal}
                            addToFavourite={addToFavourite}
                            toTrash
                        />
                    )
                })}
        </div>
    )
}

export default Favourites
