import './App.scss'

import React, {useEffect, useState} from 'react'
import axios from 'axios'
import Button from './components/Button/Button'
import Header from './components/Header/Header'
import Modal from './components/Modal/Modal'
import Routes from './routes/Routes'

const favouritesFromLS = () => {
    const favouritesLS = localStorage.getItem('Favourite')
    return favouritesLS ? JSON.parse(favouritesLS) : []
}

const App = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [items, setItems] = useState([])
    const [thisItem, setThisItem] = useState({})
    const [showModal, setShowModal] = useState(false)
    const [favourites, setFavourites] = useState(favouritesFromLS())
    const [modalAction, setModalAction] = useState('Add to cart')
    
    const saveThisItem = (item) => {
        setThisItem(item)
    }
    
    const deleteThisItem = (item) => {
        const cart = JSON.parse(localStorage.getItem('Cart'))
        
        delete cart[item.article]
        
        if (Object.keys(cart).length === 0) {
            localStorage.removeItem('Cart')
        } else {
            localStorage.setItem('Cart', JSON.stringify(cart))
        }
        closeModal()
    }
    
    const openModal = (action) => {
        if (action === 'Add to cart') {
            setModalAction('Add to cart')
        }
        if (action === 'Remove from cart') {
            setModalAction('Remove from cart')
        }
        setShowModal(true)
    }
    
    const closeModal = () => {
        setShowModal(false)
    }
    
    const addToCart = (article) => {
        if (localStorage.getItem('Cart')) {
            const cart = JSON.parse(localStorage.getItem('Cart'))
            
            if (Object.keys(cart).includes(article)) {
                cart[article]++
            } else {
                cart[article] = 1
            }
            
            localStorage.setItem('Cart', JSON.stringify(cart))
        } else {
            const cart = {}
            cart[article] = 1
            localStorage.setItem('Cart', JSON.stringify(cart))
        }
        closeModal()
    }
    
    const addToFavourite = (article) => {
        let newFavourites
        if (favourites.includes(article)) {
            newFavourites = favourites.filter((favourite) => {
                return favourite !== article
            })
        } else {
            newFavourites = [...favourites, article]
        }
        setFavourites(newFavourites)
        localStorage.setItem('Favourite', JSON.stringify(newFavourites))
    }
    
    useEffect(() => {
        axios('./items.json').then((res) => {
            res.data.forEach((card) => {
                card.favourite =
                    localStorage.getItem('Favourite') &&
                    JSON.parse(localStorage.getItem('Favourite')).includes(card.article)
            })
            setItems(res.data)
            setIsLoading(false)
        })
    }, [])
    
    if (isLoading) {
        return <div className='App'>Loading... Don`t go anywhere</div>
    }
    
    return (
        <div className='App'>
            <Header>
            </Header>
            <Routes
                items={items}
                favourites={favourites}
                saveThisItem={saveThisItem}
                deleteThisItem={deleteThisItem}
                openModal={openModal}
                addToFavourite={addToFavourite}
                modalAction={modalAction}
            />
            {showModal && (
                <Modal
                    text={`${modalAction} '${thisItem.name}'?`}
                    closeButton={true}
                    closeButtonOnClick={closeModal}
                    header={`${modalAction} '${thisItem.name}'`}
                    actions={[
                        <Button
                            key={1}
                            onClick={() => {
                                if (modalAction === 'Add to cart') {
                                    addToCart(thisItem.article)
                                }
                                if (modalAction === 'Remove from cart') {
                                    deleteThisItem(thisItem)
                                }
                            }}
                            className='modal-button'
                            text='Yes'
                        />,
                        <Button
                            key={2}
                            onClick={closeModal}
                            className='modal-button'
                            text='Cancel'
                        />,
                    ]}
                />
            )}
        </div>
    )
}

export default App
