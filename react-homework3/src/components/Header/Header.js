import React from 'react'
import {NavLink} from 'react-router-dom'
import './Header.scss'
import logo from './logo.png'
import trash from './trash.png'
import hearth from './hearth.png'

const Header = () => {
    return (
        <div className='header'>
            <div>
                <NavLink to='/main'>
                    <img className='logo' src={logo} alt={'logo'}/>
                </NavLink>
            </div>
            <div>
                <NavLink to='/favourites'>
                    <img className='hearth' src={hearth} alt={'favourites'}/>
                </NavLink>
                <NavLink to='/trash'>
                    <img className='trash' src={trash} alt={'trash'}/>
                </NavLink>
            </div>
        </div>
    )
}

export default Header
