import React, {Component} from 'react'
import './Modal.scss'

export default class Modal extends Component {
    render() {
        const {
            header,
            closeButton,
            text,
            actions,
            close,
        } = this.props;
        return (
            <div
                className='modal-background'
                onClick={close}
            >
                <div onClick={(e)=> {e.stopPropagation()}}
                    className='modal__content'>
                    <header className='modal__content-header'>
                        <div>{header}</div>
                        {closeButton && (
                            <div onClick={close} className='modal__close-btn'>
                                &times;
                            </div>
                        )}
                    </header>
                    <div>
                        <p className='modal__content-text'>{text}</p>
                        <div className='modal__buttons'>{actions}</div>
                    </div>
                </div>
            </div>
        )
    }
}
